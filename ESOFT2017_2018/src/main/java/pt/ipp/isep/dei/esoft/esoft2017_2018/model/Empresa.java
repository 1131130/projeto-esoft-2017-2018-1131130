/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.esoft2017_2018.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Empresa
{
    private final List<Equipamento> m_lstEquipamentos;
    

    public Empresa()
    {
        this.m_lstEquipamentos = new ArrayList<Equipamento>();
        
        fillInData();
    }
    
    private void fillInData()
    {
        // Dados de Teste
        //Preenche alguns Terminais
        for(Integer i=1;i<=4;i++)
            addEquipamento(new Equipamento("Equipamento " + i.toString(),"0"+i.toString(),"",""));
        
        //Preencher outros dados aqui
        
    }
    
    public Equipamento novoEquipamento()
    {
        return new Equipamento();
    }
    
    public boolean validaEquipamento(Equipamento e)
    {
        boolean bRet = false;
        if (e.valida())
        {
           // Escrever aqui o código de validação
        
           //
           bRet = true; 
        }
        return bRet;
    }
    
    public boolean registaEquipamento(Equipamento e)
    {
        if (this.validaEquipamento(e))
        {
           return addEquipamento(e);
        }
        return false;
    }
    
    private boolean addEquipamento(Equipamento e)
    {
        return m_lstEquipamentos.add(e);
    }
    
    
    
    public List<Equipamento> getListaEquipamentos()
    {
        return this.m_lstEquipamentos;
    }

    public Equipamento getEquipamento(String sEquipamento)
    {
        for(Equipamento term : this.m_lstEquipamentos)
        {
            if (term.isIdentifiableAs(sEquipamento))
            {
                return term;
            }
        }
        
        return null;
    }
    
}
    
    
