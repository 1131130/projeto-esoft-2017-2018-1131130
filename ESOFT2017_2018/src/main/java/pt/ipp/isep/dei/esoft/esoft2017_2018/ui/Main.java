/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.esoft2017_2018.ui;

import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Empresa;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Main
{
    public static void main(String[] args)
    {
        try
        {
            Empresa empresa = new Empresa();

            MenuUI uiMenu = new MenuUI(empresa);

            uiMenu.run();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
}
